import chapters.ch1.ex2.ExerciseTwoView
import chapters.ch1.ex4.ExerciseFourView
import chapters.ch2.ex1.DiceToCoinView
import chapters.ch2.ex10.ExponentationView
import chapters.ch2.ex11.ModulusExponentiationView
import chapters.ch2.ex12.GCDStepsView
import chapters.ch2.ex4.FairDiceView
import chapters.ch2.ex5.RandomArrayValuesView
import chapters.ch2.ex6.PokerView
import chapters.ch2.ex7.TwoDicesView
import javafx.scene.control.Label
import javafx.scene.control.TreeItem
import javafx.scene.layout.Region
import tornadofx.*
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

fun main(args: Array<String>) {
    launch<Application>(args)
}

class Application : App(ApplicationView::class)

class ApplicationView : View() {
    var centerView: Region = Region()

    override val root = borderpane {
        left {
            treeview<Label> {}.replaceWith(MenuView(centerView).root)
        }
        center = centerView
    }

    init {
        with(centerView){
            label { "Choose exercise" }

            minHeight = 400.0
            minWidth = 400.0
        }
    }
}

class MenuView(targetView: Region) : View() {
    private val menu = mapOf(
        "Chapter 1" to listOf(
            MenuItem("Exercise 2", ExerciseTwoView::class, targetView),
            MenuItem("Exercise 4", ExerciseFourView::class, targetView)
        ),
        "Chapter 2" to listOf(
            MenuItem("Exercise 1", DiceToCoinView::class, targetView),
            MenuItem("Exercise 4", FairDiceView::class, targetView),
            MenuItem("Exercise 5", RandomArrayValuesView::class, targetView),
            MenuItem("Exercise 6", PokerView::class, targetView),
            MenuItem("Exercise 7", TwoDicesView::class, targetView),
            MenuItem("Exercise 10", ExponentationView::class, targetView),
            MenuItem("Exercise 11", ModulusExponentiationView::class, targetView),
            MenuItem("Exercise 12", GCDStepsView::class, targetView)
        )
    )

    override val root = treeview<Label> {
        root = TreeItem(Label("Chapters"))

        populate { parent ->
            if (parent == root) {
                menu.keys.map { Label(it) }
            } else {
                menu[parent.value.text]
            }
        }
    }
}

class MenuItem(text: String, view: KClass<out View>, target: Region) : Label() {
    init {
        this.text = text
        this.setOnMouseClicked {
            val instance = view.createInstance()
            instance.currentWindow?.sizeToScene()
            target.replaceChildren(instance.root)
        }
    }
}