package chapters.ch1.ex2

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.layout.HBox
import tornadofx.*

class ExerciseTwoView : View() {
    override var root = HBox()

    private val second = 1000000.0
    private val minute = second * 60
    private val hour = minute * 60
    private val day = hour * 24
    private val week = day * 7
    private val year = day * 365.25

    private val functions = listOf(
        Function("Second", second),
        Function("Minute", minute),
        Function("Hour", hour),
        Function("Day", day),
        Function("Week", week),
        Function("Year", year)
    ).observable()

    init {
        title = "Algorithms complexity"
        with(root) {
            tableview(functions) {
                column("Time", Function::name)
                column("log N", Function::log)
                column("sqrt(N)", Function::sqrt)
                column("N", Function::n)
                column("N^2", Function::square)
                column("2^N", Function::pow)
                column("N!", Function::factorial)

                smartResize()
            }
        }
    }
}

class Function(name: String, count: Double) {
    private val nameProperty = SimpleStringProperty(name)
    var name: String by nameProperty

    val log = SimpleDoubleProperty(Math.pow(2.0, count))
    val sqrt = SimpleDoubleProperty(count * count)
    val n = SimpleDoubleProperty(count)
    val square = SimpleDoubleProperty(Math.sqrt(count))
    val pow = SimpleDoubleProperty(Math.log10(count))
    val factorial = SimpleDoubleProperty(inverseFactorial(count).toDouble())
}

private fun inverseFactorial(value: Double): Int {
    var i = 1
    while (true) {
        if (factorial(i) > value) return i - 1
        i++
    }
}

private fun factorial(n: Int): Double {
    var result = 1.0
    for (i in 2 until n) result *= i.toDouble()
    return result
}