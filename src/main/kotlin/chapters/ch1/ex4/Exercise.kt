package chapters.ch1.ex4

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.scene.layout.VBox
import tornadofx.*

class ExerciseFourView : View() {
    override var root = VBox()
    private val results = getResult().observable()

    init {
        root = vbox {
            label("Использовать алгоритм со сложностью O(N^3/75-N^2/4+N+10) вместо O(N/2+8)")
            label("целесообразно только при Result = true")
            tableview(results) {
                column("N", ResultModel::n)
                column("Result", ResultModel::result)

                smartResize()
            }
        }
    }
}

class ResultModel(n: Int, result: Boolean) {
    private val nProperty = SimpleIntegerProperty(n)
    var n: Int by nProperty

    private val resultProperty = SimpleBooleanProperty(result)
    var result: Boolean by resultProperty
}

fun getResult(): MutableList<ResultModel> {
    val result = mutableListOf<ResultModel>()

    for (i in 1..20) {
        result.add(ResultModel(i, calculate(i)))
    }

    return result
}

fun calculate(i: Int): Boolean {
    return (i * i * i / 75.0f - i * i / 4 + i + 10) < (i / 2.0f + 8)
}