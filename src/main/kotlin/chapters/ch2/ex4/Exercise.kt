package chapters.ch2.ex4

import javafx.beans.property.SimpleStringProperty
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class FairDiceView : View() {
    override var root = VBox()
    private var result = SimpleStringProperty()
    private var resultText by property("Rolled: ")

    init {
        root = vbox {
            button("Roll dice", null) {
                setOnMouseClicked {
                    result.value = rollFairDice().toString()
                }
            }
            hbox {
                label().textProperty().bind(resultText.toProperty())
                label().textProperty().bind(result)
            }

            minHeight = 400.0
            minWidth = 400.0
        }
    }
}

fun rollFairDice(): Int {
    var result = rollDice()

    while (result.groupBy { i -> i }.count() != 6) {
        result = rollDice()
    }

    return result[0]
}

fun rollDice() = Array(6) { Random.nextInt(1, 7) }
