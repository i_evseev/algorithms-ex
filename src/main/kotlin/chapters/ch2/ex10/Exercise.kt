package chapters.ch2.ex10

import javafx.beans.property.SimpleFloatProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.collections.set

open class ExponentationView : View() {
    override var root = VBox()
    var number = SimpleFloatProperty()
    var exponent = SimpleIntegerProperty()
    var result = SimpleFloatProperty()

    init {
        root = vbox {
            hbox {
                textfield(number)
                textfield(exponent)
                button {
                    text = "Calculate"
                    setOnMouseClicked {
                        result.set(number.value.exponentiation(exponent.value))
                    }
                }
            }
            label(result)
        }
    }
}

fun Number.exponentiation(exponent: Int): Float {
    var exponent = exponent
    var stageExponent = 1
    val stageResults = mutableMapOf(
        stageExponent to this.toFloat()
    )

    stageExponent *= 2

    do {
        stageResults[stageExponent] = stageResults[stageExponent / 2]!! * stageResults[stageExponent / 2]!!
        stageExponent *= 2
    } while (stageExponent <= exponent)

    var result = 1f

    do {
        stageExponent /= 2
        if (exponent - stageExponent < 0) continue
        result *= stageResults[stageExponent]!!
        exponent -= stageExponent
    } while (exponent != 0)

    return result
}
