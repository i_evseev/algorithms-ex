package chapters.ch2.ex12

import javafx.scene.chart.NumberAxis
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class GCDStepsView : View() {
    override var root = VBox()
    private val results = mutableListOf<Pair<Int, Float>>()
    private val logarithms = mutableListOf<Pair<Float, Double>>()

    init {
        var first = 0
        var second = 0

        for (i in 0..10000) {
            first = Random.nextInt(-10000000, 10000000)
            second = Random.nextInt(-10000000, 10000000)

            results.add(Pair(first gcdsteps second, (first + second) / 2f))
        }

        var j = 0.01f

        while (j <= 30) {
            logarithms.add( Pair( j, Math.log( j.toDouble() ) * 1000000 * 4 ) )
            j += 0.05f
        }

        root = vbox {
            linechart("Chart", NumberAxis(), NumberAxis()) {
                series("Results") {
                    results.forEach {
                        data(it.first, it.second)
                    }
                }
                series("log") {
                    logarithms.forEach {
                        data(it.first, it.second)
                    }
                }
            }
        }
    }
}

infix fun Int.gcdsteps(second: Int): Int {
    var first = this
    var second = second
    var steps = 0

    while (second != 0) {
        steps++
        var remainder = first % second
        first = second
        second = remainder
    }

    return steps
}
