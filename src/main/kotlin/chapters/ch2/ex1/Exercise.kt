package chapters.ch2.ex1

import javafx.beans.property.SimpleStringProperty
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class DiceToCoinView : View() {
    override var root = VBox()
    private var flipCoinResult = SimpleStringProperty()
    private var resultText by property("Coin: ")

    init {
        root = vbox {
            button("Flip coin", null) {
                setOnMouseClicked {
                    flipCoinResult.setValue(flipCoin { rollDice() })
                }
            }
            hbox {
                label().textProperty().bind(resultText.toProperty())
                label().textProperty().bind(flipCoinResult)
            }

            minHeight = 400.0
            minWidth = 400.0
        }
    }
}

fun SimpleStringProperty.setValue(value: Boolean) {
    set(if (value) "Head" else "Tail")
}

fun flipCoin(rollDice: () -> Int): Boolean {
    return rollDice() % 2 == 0
}

fun rollDice(): Int {
    return Random.nextInt(1, 7)
}