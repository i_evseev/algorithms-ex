package chapters.ch2.ex7

import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.NumberAxis
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class TwoDicesView : View() {
    override var root = VBox()
    var results = mutableMapOf<Int, Int>().observable()

    init {
        for (x in 0..10000) {
            val scores = rollDicesTwice()
            when (results[scores]) {
                null -> results[scores] = 1
                is Int -> results.replace(scores, results.getValue(scores) + 1)
            }
        }

        root = vbox {
            hbox {
                barchart("Dice rolls", CategoryAxis(), NumberAxis()) {
                    series("Current rolls result") {
                        results.toSortedMap().observable().forEach {
                            data(it.key.toString(), it.value)
                        }
                    }
                }

                minHeight = 400.0
                minWidth = 400.0
                autosize()
            }
        }
    }
}

fun rollDicesTwice() = rollDice() + rollDice()

fun rollDice() = Random.nextInt(1, 7)
