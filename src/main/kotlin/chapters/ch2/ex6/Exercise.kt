package chapters.ch2.ex6

import javafx.collections.ObservableList
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class PokerView : View() {
    override var root = VBox()
    private var deck = generateDeck()
    private var players = mutableMapOf(
        0 to mutableListOf<String>().observable(),
        1 to mutableListOf<String>().observable(),
        2 to mutableListOf<String>().observable(),
        3 to mutableListOf<String>().observable(),
        4 to mutableListOf<String>().observable()
    ).observable()
    private var resultText by property("Result hands: ")

    init {
        root = vbox {
            button("Deal cards", null) {
                setOnMouseClicked {
                    dealCards(randomizeDeck(deck), players)
                }
            }
            hbox {
                label().textProperty().bind(resultText.toProperty())
                for (player in 0 until players.count()) {
                    vbox {
                        listview(players[player])
                        autosize()
                    }
                    autosize()
                }
            }
        }
    }
}

fun generateDeck(): Array<String> = Array(52) { i: Int -> "Card $i" }

fun randomizeDeck(deck: Array<String>): Array<String> {
    val cardsInDeck = deck.count()

    for (position in 0 until cardsInDeck) {
        val newPosition = Random.nextInt(0, 52)
        val card = deck[newPosition]
        deck[newPosition] = deck[position]
        deck[position] = card
    }

    return deck
}

fun dealCards(deck: Array<String>, players: MutableMap<Int, ObservableList<String>>) {
    val playersCount = players.count()
    players.map { it.value.clear() }
    for (cardPosition in 0 until playersCount * 5 step playersCount) {
        players.forEach {
            it.value.add(deck[it.key + cardPosition])
        }
    }
}

