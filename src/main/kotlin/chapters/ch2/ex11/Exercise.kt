package chapters.ch2.ex11

import chapters.ch2.ex10.ExponentationView
import tornadofx.*
import kotlin.collections.set

class ModulusExponentiationView : ExponentationView() {
    init {
        root = vbox {
            hbox {
                textfield(number)
                textfield(exponent)
                button {
                    text = "Calculate"
                    setOnMouseClicked {
                        result.set(number.value.modulusExponentation(exponent.value))
                    }
                }
            }
            label(result)
        }
    }
}

fun Number.modulusExponentation(exponent: Int): Float {
    var exponent = exponent
    var stageExponent = 1
    var value = this.toFloat()

    if (value < 0) {
        value = 0 - value
    }

    val stageResults = mutableMapOf(
        stageExponent to value
    )

    stageExponent *= 2

    do {
        stageResults[stageExponent] = stageResults[stageExponent / 2]!! * stageResults[stageExponent / 2]!!
        stageExponent *= 2
    } while (stageExponent <= exponent)

    var result = 1f

    do {
        stageExponent /= 2
        if (exponent - stageExponent < 0) continue
        result *= stageResults[stageExponent]!!
        exponent -= stageExponent
    } while (exponent != 0)

    return result
}
