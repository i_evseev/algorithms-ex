package chapters.ch2.ex5

import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.random.Random

class RandomArrayValuesView : View() {
    override var root = VBox()
    private var result = getWinners(generateArrayOfParticipants()).toList().observable()
    private var resultText by property("Rolled: ")

    init {
        root = vbox {
            button("Select winners", null) {
                setOnMouseClicked {
                    result.setAll(getWinners(generateArrayOfParticipants()).toList())
                }
            }
            hbox {
                label().textProperty().bind(resultText.toProperty())
                listview(result)
            }

            minHeight = 400.0
            minWidth = 400.0
        }
    }
}

fun generateArrayOfParticipants(): Array<String> = Array(10000) { i: Int -> "Participant $i" }

fun getWinners(participants: Array<String>): Array<String> {
    val participantsCount = participants.count()
    return Array(5) { participants[Random.nextInt(0, participantsCount + 1)] }
}
